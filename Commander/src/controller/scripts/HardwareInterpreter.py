#!/usr/bin/env python
import math
import rospy
from controller.msg import Joystick
from controller.msg import Arm_absolute
from controller.msg import Arm_relative
from controller.msg import Arm_height
from sensor_msgs.msg import Joy
from std_msgs.msg import Bool
from std_msgs.msg import Int8
from std_msgs.msg import UInt8
from std_msgs.msg import Int16
from std_msgs.msg import UInt16
from controller.msg import drive
import sys
import time

global pumpOn, lastStatePump, lastStateUp, J3Up, lastStateLB, lastStateLS, lastStateRS, lastStateB
global lastStateX, index, lastStateRB, lastStateY, factor, lastStatePI1, lastStatePI0
global speedPub, pumpPub, upPub, stoPub, colPub, haiPub
global heightAtom, zAxisLimitSwitchGroundOffset, jointLVL2Length, jointLVL1Length, zAxisHeight, jointLVL3Length, zPerStep

def swPump(on):
    global pumpOn, pumpPub
    pumpOn = on
    pumpPub.publish(Bool(pumpOn))

def JoystickToWheel(x, y):
    m1,m2 = 0, 0
    if y == 0:
        return x, -x
    if x > 0:
        if y > 0:
            m1 = math.sqrt(math.pow(x, 2)+math.pow(y, 2))
        else:
            m1 = -math.sqrt(math.pow(x, 2)+math.pow(y, 2))
        m2 = y
    else:
        m1 = y
        if y > 0:
            m2 = math.sqrt(math.pow(x, 2)+math.pow(y, 2))
        else:
            m2 = -math.sqrt(math.pow(x, 2)+math.pow(y, 2))
    
    m2 *= 1.124
    return m1,m2

def moveZAxis(hai, absolut):
    global haiPub, J3Up, zAxisHeight
    haiMSG = Arm_height()
    haiMSG.absolute = absolut
    haiMSG.heightMM = hai
    haiPub.publish(haiMSG)
    distToMove = 0
    if absolut:
        distToMove = hai-zAxisHeight
        if J3Up:
            distToMove-=jointLVL3Length
    else:
        distToMove = hai
    
    zAxisHeight += distToMove
    stepsToDo = abs(distToMove) / zPerStep
    time.sleep((0.7/1000)*stepsToDo)

def goToPoint(x, y):
    global relPub
    relMSG = Arm_relative()
    relMSG.x = x
    relMSG.y = y
    relPub.publish(relMSG)
    time.sleep(0.8)

def armAbs(lvl1, lvl2, dur=300):
    global armPub
    absMSG = Arm_absolute()
    absMSG.angleLVL1 = lvl1
    absMSG.angleLVL2 = lvl2
    absMSG.dur = dur
    armPub.publish(absMSG)
    time.sleep(dur/1000)

def collectStorage(indexIN):
    global heightAtom
    moveZAxis(60 + heightAtom + (indexIN * heightAtom) + 20, True)
    goToPoint(120, -40)
    swPump(True)
    moveZAxis(60 + heightAtom + (indexIN * heightAtom), True)
    time.sleep(0.3)
    moveZAxis(10, False)

def pushIn(dist):
    global jointLVL3Length, jointLVL2Length, jointLVL1Length
    correctedDist = dist-jointLVL2Length-jointLVL3Length #calcualte mathamatically
    radiansAlapha = math.asin(correctedDist/jointLVL1Length)
    degreesAlapha = (radiansAlapha*180.0/math.pi)
    offSetDist = math.sqrt(math.pow(jointLVL1Length, 2)-math.pow(correctedDist, 2))

    jointLVL1Angle = degreesAlapha;     #translate to relevant instructions
    jointLVL2Angle = (90-degreesAlapha);

    #execute instructions
    armAbs(jointLVL1Angle, jointLVL2Angle, 600)

def joystickCB(data):
    global lastStatePump, lastStateUp, lastStateX, lastStateLS, lastStateB, lastStatePI0, lastStateY, lastStateRB, lastStateLB, lastStateRS, lastStatePI1
    global speedPub, pumpPub, armPub, relPub, colPub, upPub, pumpOn, haiPub, stoPub
    
    msg = drive()
    rt = (-data.axes[5] + 1) * (factor/2)
    lt = (-data.axes[2] + 1) * (factor/2)
    if (lt > 0 and rt == 0):
        msg.x, msg.y = JoystickToWheel(data.axes[3] * factor, -lt)
    elif (rt > 0 and lt == 0):
        msg.x, msg.y = JoystickToWheel(data.axes[3] * factor, rt)
    else:
        msg.x, msg.y = JoystickToWheel(data.axes[3] * factor, 0)
    speedPub.publish(msg)

    if data.buttons[9] == 1 and not lastStateLS:
        goToPoint(85, 85)
        lastStateLS = True
    elif data.buttons[9] == 0:
        lastStateLS = False

    if data.buttons[1] == 1 and not lastStatePump:
        pumpOn = not pumpOn
        lastStatePump = True
    elif data.buttons[1] == 0:
        lastStatePump = False
    
    msgPuO = Bool()
    msgPuO.data = pumpOn
    pumpPub.publish(msgPuO)

    if data.buttons[0] == 1 and not lastStateUp:
        J3Up = not J3Up
        lastStateUp = True
    elif data.buttons[0] == 0:
        lastStateUp = False

    if data.buttons[10] == 1 and not lastStateRS:
        armAbs(90, 0)
        J3Up = True
        msg3U = Bool()
        msg3U.data = J3Up
        upPub.publish(msg3U)
        moveZAxis(210, True)
        lastStateRS = True
    elif data.buttons[10] == 0:
        lastStateRS = False

    if data.buttons[6] == 1 and not lastStateB:
        J3Up = True
        msg3U = Bool()
        msg3U.data = J3Up
        upPub.publish(msg3U)
        armAbs(0, 0)
        moveZAxis(69, True)
        lastStateB = True
    elif data.buttons[6] == 0:
        lastStateB = False

    if data.axes[6] > 0.5 and not lastStatePI0:
        pushIn(140)
        J3Up = True
        msg3U = Bool()
        msg3U.data = J3Up
        upPub.publish(msg3U)
        moveZAxis(210, True)
        lastStatePI0 = True
    elif data.axes[6] < 0.5:
        lastStatePI0 = False

    if data.axes[6] < -0.5 and not lastStatePI1:
        pushIn(210)
        lastStatePI1 = True
    elif data.axes[6] > -0.5:
        lastStatePI1 = False
    
    msg3U = Bool()
    msg3U.data = J3Up
    upPub.publish(msg3U)

    if data.buttons[3] == 1 and not lastStateX:
        if zAxisHeight < 55+jointLVL3Length:
            moveZAxis(60+jointLVL3Length, True)
        msgCol = UInt8()
        msgCol.data = 255
        J3Up = False
        msg3U = Bool()
        msg3U.data = J3Up
        upPub.publish(msg3U)
        time.sleep(0.7)
        goToPoint(85, 85)
        pumpOn = True
        msgPuO = Bool()
        msgPuO.data = pumpOn
        pumpPub.publish(msgPuO)
        moveZAxis(heightAtom, True)
        lastStateX = True
    elif data.buttons[3] == 0:
        lastStateX = False

    if data.buttons[2] == 1 and not lastStateY:
        msgSto = UInt8()
        if index >= 0:
            J3Up = False
            msg3U = Bool()
            msg3U.data = J3Up
            upPub.publish(msg3U)
            msgSto.data = index
            time.sleep(0.3)
            stoPub.publish(msgSto)
        index = index + 1
        lastStateY = True
    elif data.buttons[2] == 0:
        lastStateY = False
    
    if data.buttons[5] == 1 and not lastStateRB:
        msgCo = UInt8()
        if index >= 0:
            index = index - 1
            J3Up = False
            msg3U = Bool()
            msg3U.data = J3Up
            upPub.publish(msg3U)
            collectStorage(index)
        lastStateRB = True
    elif data.buttons[5] == 0:
        lastStateRB = False
    
    if data.buttons[4] == 1 and not lastStateLB:
        if factor == 30:
            factor = 75
        elif factor == 75:
            factor = 150
        elif factor == 150:
            factor = 30
        lastStateLB = True
    elif data.buttons[4] == 0:
        lastStateLB = False

def initVars():
    global heightAtom, zAxisLimitSwitchGroundOffset, zAxisHeight, zPerStep  # everything for Z-Axis
    heightAtom = 25
    zAxisLimitSwitchGroundOffset = 22
    zAxisHeight = zAxisLimitSwitchGroundOffset
    zPerStep  = 8.0 * 1.8 * 3.0 / 360.0

    global jointLVL3Length, jointLVL2Length, jointLVL1Length                # joint data
    jointLVL1Length = 80.0
    jointLVL2Length = 44.5
    jointLVL3Length = 88.5

    global lastStateLS, lastStateRS                                         # sticks
    lastStateLS = False
    lastStateRS = False

    global lastStateRB, lastStateLB, lastStateB                             # auxillary
    lastStateB = False
    lastStateRB = False
    lastStateLB = False

    global lastStateY, lastStateX, lastStatePump, lastStateUp               # the four buttons
    lastStateX = False
    lastStatePump = False
    lastStateY = False
    lastStateUp = False

    global lastStatePI0, lastStatePI1                                       # cross
    lastStatePI1 = False
    lastStatePI0 = False

    global index, factor, J3Up, pumpOn                                      # global data
    J3Up = False
    pumpOn = False
    factor = 30
    index = 0    

def main():
    global speedPub, pumpPub, armPub, relPub, colPub, upPub, pumpOn, haiPub, stoPub

    initVars()
    
    rospy.init_node('HardwareInterpreter')
    speedPub = rospy.Publisher('drive_speed', drive, queue_size=1)
    pumpPub = rospy.Publisher('pump_on', Bool, queue_size=1)
    upPub = rospy.Publisher('joint_LVL3_up', Bool, queue_size=1)
    colPub = rospy.Publisher('collect', UInt8, queue_size=1)
    stoPub = rospy.Publisher('store', UInt8, queue_size=1)
    haiPub = rospy.Publisher('height_arm', Arm_height, queue_size=1)
    armPub = rospy.Publisher('absolute_arm', Arm_absolute, queue_size=1)
    relPub = rospy.Publisher('relative_arm', Arm_relative, queue_size=1)
    rospy.Subscriber("joy", Joy, joystickCB)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    main()